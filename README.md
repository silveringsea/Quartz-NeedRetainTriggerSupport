#Quartz-NeedRetainTriggerSupport

任何AbstractTrigger的子类只要通过NeedRetainTriggerSupport包装一下，则可以实现保留已经结束的Trigger！

只需要3行代码
trigger.setJobName("Hello Job"); // 必须要设置JobName，否则NeedRetainTriggerSupport无法支持

NeedRetainTriggerSupport triggerSupport = new NeedRetainTriggerSupport(trigger); // 包装你的 trigger
triggerSupport.setNeedRetain(true); // 默认已为true

scheduler.scheduleJob(jobDetail, triggerSupport);
