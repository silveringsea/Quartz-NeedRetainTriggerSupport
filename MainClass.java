package com.rocbin.quartz.test;

import com.rocbin.quartz.test.jobs.HelloJob;
import com.rocbin.quartz.trigger.NeedRetainTriggerSupport;
import org.quartz.*;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.SimpleTriggerImpl;

import java.util.Date;

/**
 * @author Rocbin 2016
 *         Created by Rocbin on 2016/11/5.
 */
public class MainClass {
    static Scheduler scheduler = null;

    public static void main(String[] args) throws SchedulerException {
        scheduler = StdSchedulerFactory.getDefaultScheduler();
        String schedulerName = scheduler.getSchedulerName();
        System.out.println(schedulerName);
        scheduler.start();

        if (scheduler.getJobDetail(JobKey.jobKey("Hello Job", "group1")) == null) {
            JobDetailImpl jobDetail = new JobDetailImpl();
            jobDetail.setJobClass(HelloJob.class);
            jobDetail.setName("Hello Job");
            jobDetail.setDescription("Hello Job test");
            SimpleTriggerImpl trigger = (SimpleTriggerImpl) TriggerBuilder
                    .newTrigger()
                    .withIdentity("trigger1", "group1")
                    .startAt(new Date())
                    .build();
            trigger.setRepeatCount(10);
            trigger.setRepeatInterval(2000);
            trigger.setJobName("Hello Job"); // 必须要设置JobName，否则NeedRetainTriggerSupport无法支持
            NeedRetainTriggerSupport triggerSupport = new NeedRetainTriggerSupport(trigger);
            triggerSupport.setNeedRetain(true);
            scheduler.scheduleJob(jobDetail, triggerSupport);

        }

//        scheduler.shutdown(true);
    }
}
